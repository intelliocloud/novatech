
public class icBusinessLogicAggregatedSales implements icIClass {
	
	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		Aggregated_Sales__c generateNewAggregatedSaleFromSource(Aggregated_Sales_Source__c source, Account sourceAccount, Contact sourceContact, Product2 sourceProduct);
		void createNewAggregatesSales(List<Aggregated_Sales__c> listNewAggregatedSales);
		void deleteAggregatesSales(List<Aggregated_Sales__c> listAggregatedSales);
		void deleteAggregatesSalesSource(List<Aggregated_Sales_Source__c> listAggregatedSalesSource);
	}

	public class Impl implements IClass {
		icRepositoryAggregatedSales.IClass repoAggregatedSales = (icRepositoryAggregatedSales.IClass) icObjectFactory.GetSingletonInstance('icRepositoryAggregatedSales');

		public Aggregated_Sales__c generateNewAggregatedSaleFromSource(Aggregated_Sales_Source__c source, Account sourceAccount, Contact sourceContact, Product2 sourceProduct) {
			Aggregated_Sales__c newAggregatedSale = copyFromSource(source);

			if(sourceAccount != null) {
				newAggregatedSale.Account__c = sourceAccount.Id;
			}
			if(sourceContact != null) {
				newAggregatedSale.Contact__c = sourceContact.Id;				
			}
			if(sourceProduct != null) {
				newAggregatedSale.Product__c = sourceProduct.Id;				
			}
			newAggregatedSale.CurrencyIsoCode = source.Currency__c;

			return newAggregatedSale;
		}

		public void createNewAggregatesSales(List<Aggregated_Sales__c> listNewAggregatedSales) {
			repoAggregatedSales.createNewAggregatesSales(listNewAggregatedSales);
		}

		public void deleteAggregatesSales(List<Aggregated_Sales__c> listAggregatedSales) {
			repoAggregatedSales.deleteAggregatesSales(listAggregatedSales);
		}

		public void deleteAggregatesSalesSource(List<Aggregated_Sales_Source__c> listAggregatedSalesSource) {
			repoAggregatedSales.deleteAggregatesSalesSource(listAggregatedSalesSource);
		}

		private Aggregated_Sales__c copyFromSource(Aggregated_Sales_Source__c source) {
			Aggregated_Sales__c newAggregatedSale = new Aggregated_Sales__c();

			newAggregatedSale.Client_Name__c = source.Client_Name__c;
			newAggregatedSale.Client_Number__c = source.Client_Number__c;
			newAggregatedSale.Currency__c = source.Currency__c;
			newAggregatedSale.CY_MTD_Budget__c = source.CY_MTD_Budget__c;
			newAggregatedSale.CY_MTD_Sales__c = source.CY_MTD_Sales__c;
			newAggregatedSale.CY_MTD_Sales_QTY__c = source.CY_MTD_Sales_QTY__c;
			newAggregatedSale.CY_YTD_Budget__c = source.CY_YTD_Budget__c;
			newAggregatedSale.CY_YTD_Sales__c = source.CY_YTD_Sales__c;
			newAggregatedSale.CY_YTD_Sales_QTY__c = source.CY_YTD_Sales_QTY__c;
			newAggregatedSale.FY_PTD_Budget__c = source.FY_PTD_Budget__c;
			newAggregatedSale.FY_PTD_Sales__c = source.FY_PTD_Sales__c;
			newAggregatedSale.FY_PTD_Sales_QTY__c = source.FY_PTD_Sales_QTY__c;
			newAggregatedSale.FY_YTD_Budget__c = source.FY_YTD_Budget__c;
			newAggregatedSale.FY_YTD_Sales__c = source.FY_YTD_Sales__c;
			newAggregatedSale.FY_YTD_Sales_QTY__c = source.FY_YTD_Sales_QTY__c;
			newAggregatedSale.LCY_MTD_Budget__c = source.LCY_MTD_Budget__c;
			newAggregatedSale.LCY_MTD_Sales__c = source.LCY_MTD_Sales__c;
			newAggregatedSale.LCY_MTD_Sales_QTY__c = source.LCY_MTD_Sales_QTY__c;
			newAggregatedSale.LCY_YTD_Sales__c = source.LCY_YTD_Sales__c;
			newAggregatedSale.LCY_YTD_Sales_QTY__c = source.LCY_YTD_Sales_QTY__c;
			newAggregatedSale.LFY_PTD_Sales__c = source.LFY_PTD_Sales__c;
			newAggregatedSale.LFY_PTD_Sales_QTY__c = source.LFY_PTD_Sales_QTY__c;
			newAggregatedSale.LFY_YTD_Sales__c = source.LFY_YTD_Sales__c;
			newAggregatedSale.LFY_YTD_Sales_QTY__c = source.LFY_YTD_Sales_QTY__c;
			newAggregatedSale.Product_Family__c = source.Product_Family__c;
			newAggregatedSale.Product_Group__c = source.Product_Group__c;
			newAggregatedSale.Product_Line__c = source.Product_Line__c;
			newAggregatedSale.Rolling_12_Months_Budget__c = source.Rolling_12_Months_Budget__c;
			newAggregatedSale.Rolling_12_Months_LY_Budget__c = source.Rolling_12_Months_LY_Budget__c;
			newAggregatedSale.Rolling_12_Months_LY_Sales__c = source.Rolling_12_Months_LY_Sales__c;
			newAggregatedSale.Rolling_12_Months_LY_Sales_QTY__c = source.Rolling_12_Months_LY_Sales_QTY__c;
			newAggregatedSale.Rolling_12_Months_Sales__c = source.Rolling_12_Months_Sales__c;
			newAggregatedSale.Rolling_12_Months_Sales_QTY__c = source.Rolling_12_Months_Sales_QTY__c;
			newAggregatedSale.Rolling_52_Weeks_Budget__c = source.Rolling_52_Weeks_Budget__c;
			newAggregatedSale.Rolling_52_Weeks_Sales__c = source.Rolling_52_Weeks_Sales__c;
			newAggregatedSale.Rolling_52_Weeks_Sales_QTY__c = source.Rolling_52_Weeks_Sales_QTY__c;
			newAggregatedSale.Sales_Rep_Name__c = source.Sales_Rep_Name__c;
			newAggregatedSale.Territory__c = source.Territory__c;

			return newAggregatedSale;
		}
	}
}