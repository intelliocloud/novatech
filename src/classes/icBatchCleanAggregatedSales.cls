global class icBatchCleanAggregatedSales implements Database.Batchable<sObject>, Schedulable, Database.Stateful {

	global List<String> logs = new List<String>();
	global Integer batchNumber = 1;

	global icBusinessLogicAggregatedSales.IClass blAggregatedSales = (icBusinessLogicAggregatedSales.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicAggregatedSales');

	global void execute(SchedulableContext sc) {
		icBatchCleanAggregatedSales thisBatchClass = new icBatchCleanAggregatedSales();
		Database.executebatch(thisBatchClass);
	}

	global Database.querylocator start(Database.BatchableContext BC) {
		logs.add('Batch Class \'icBatchCleanAggregatedSales\' started at : ' + Datetime.now());
		System.debug('start logs : ' + logs);
		System.debug('start logs : ' + batchNumber);

		String query = 'SELECT Id FROM Aggregated_Sales__c';
		if(Test.isRunningTest()) {
			query += ' LIMIT 10';
		}
		
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Aggregated_Sales__c> scope) {
		logs.add('Batch Class \'icBatchCleanAggregatedSales\' executing batch ' + batchNumber + ' at : ' + Datetime.now());
		System.debug('execute logs : ' + logs);
		System.debug('execute logs : ' + batchNumber);

		blAggregatedSales.deleteAggregatesSales(scope);
	}

	global void finish(Database.BatchableContext BC) {
		logs.add('Batch Class \'icBatchCleanAggregatedSales\' completed at : ' + Datetime.now());
		System.debug('finish logs : ' + logs);
		System.debug('finish logs : ' + batchNumber);

		Messaging.SingleEmailMessage emailOut = new Messaging.SingleEmailMessage(); 

		String emailBodyHTML = '';
		for(String log : logs)
		{
			emailBodyHTML += log + '<br>';
		}

		emailOut.setSubject('Batch Class \'icBatchCleanAggregatedSales\' Execution Complete');
		emailOut.setSenderDisplayName('Apex Batch');
		emailOut.setToAddresses(new String[] {'denis.roy@in-cloud.ca'});
		emailOut.setHTMLBody(emailBodyHTML);        

		Messaging.sendEmail(new List<Messaging.Email>{emailOut});

		icBatchProcessAggregatedSalesSource thisBatchClass = new icBatchProcessAggregatedSalesSource();
		Database.executebatch(thisBatchClass);
	}
}