public class icRepositoryAccount implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Account> getListAccountByExternalId(Set<String> setExternalIds);
	}

	public class Impl implements IClass {
		
		public List<Account> getListAccountByExternalId(Set<String> setExternalIds) {
			return [SELECT Id, Oracle_Site_ID__c FROM Account WHERE Oracle_Site_ID__c IN :setExternalIds];
		}
	}
}