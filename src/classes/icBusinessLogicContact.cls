public class icBusinessLogicContact implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Contact> getListContactForNovatech();
		Contact getContactByNameString(List<Contact> listContact, String fullName);
	}

	public class Impl implements IClass {
		icRepositoryContact.IClass repoContacts = (icRepositoryContact.IClass) icObjectFactory.GetSingletonInstance('icRepositoryContact');
		
		public List<Contact> getListContactForNovatech() {
			return repoContacts.getListContactForNovatech();
		}

		public Contact getContactByNameString(List<Contact> listContact, String fullName) {
			Contact returnContact = null;

			//System.debug('fullName : ' + fullName);

			List<String> nameSplit = fullName.split('¸');
			String lastName = nameSplit[0].trim().toLowerCase();
			String firstName;
			//System.debug('nameSplit (' + nameSplit.size() + ') : ' + nameSplit);
			//System.debug('Last Name : ' + lastName);

			List<Contact> contactMatchLastName = new List<Contact>();
			for(Contact thisContact : listContact) {
				//System.debug('Compare : ' + thisContact.LastName);
				if(thisContact.LastName.toLowerCase() == lastName) {
					contactMatchLastName.add(thisContact);
				}
			}

			//System.debug('Last Name Match : ' + contactMatchLastName);

			if(contactMatchLastName.size() > 0) {
				if(nameSplit.size() > 1) {
					firstName = nameSplit[1].trim().toLowerCase();

					//System.debug('First Name : ' + firstName);

					for(Contact thisContact : contactMatchLastName) {
						//System.debug('Compare : ' + thisContact.FirstName);
						if(thisContact.FirstName.toLowerCase() == firstName) {
							returnContact = thisContact;
							break;
						}
					}
				} else {
					returnContact = contactMatchLastName[0];
				}
			}

			return returnContact;
		}
	}
}