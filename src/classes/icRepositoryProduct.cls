public class icRepositoryProduct implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Product2> getListAllProducts();
	}

	public class Impl implements IClass {
		
		public List<Product2> getListAllProducts() {
			return [SELECT Id, Name, Family, Product_Line__c, SegmentationDetail__c FROM Product2];
		}
	}
}

