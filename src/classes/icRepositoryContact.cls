public class icRepositoryContact implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Contact> getListContactForNovatech();
	}

	public class Impl implements IClass {
		
		public List<Contact> getListContactForNovatech() {
			return [SELECT Id, FirstName, LastName FROM Contact WHERE Account.Name = 'Groupe Novatech'];
		}
	}
}