global class icBatchProcessAggregatedSalesSource implements Database.Batchable<sObject>, Schedulable, Database.Stateful {

	global List<String> logs = new List<String>();
	global Integer batchNumber = 0;
	global Integer totalCreated = 0;
	global Integer totalDeleted = 0;

	global icBusinessLogicAggregatedSales.IClass blAggregatedSales = (icBusinessLogicAggregatedSales.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicAggregatedSales');
	global icBusinessLogicAccount.IClass blAccounts = (icBusinessLogicAccount.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicAccount');
	global icBusinessLogicContact.IClass blContacts = (icBusinessLogicContact.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicContact');
	global icBusinessLogicProduct.IClass blProducts = (icBusinessLogicProduct.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicProduct');

	global void execute(SchedulableContext sc) {
		icBatchProcessAggregatedSalesSource thisBatchClass = new icBatchProcessAggregatedSalesSource();
		Database.executebatch(thisBatchClass);
	}

	global Database.querylocator start(Database.BatchableContext BC) {
		logs.add('Batch Class \'icBatchProcessAggregatedSalesSource\' started at : ' + Datetime.now());
		
		String query = 'SELECT Id, Client_Name__c, Client_Number__c, Currency__c, CY_MTD_Budget__c, CY_MTD_Sales__c, CY_MTD_Sales_QTY__c, CY_YTD_Budget__c, CY_YTD_Sales__c, CY_YTD_Sales_QTY__c, FY_PTD_Budget__c, FY_PTD_Sales__c, FY_PTD_Sales_QTY__c, FY_YTD_Budget__c, FY_YTD_Sales__c, FY_YTD_Sales_QTY__c, LCY_MTD_Budget__c, LCY_MTD_Sales__c, LCY_MTD_Sales_QTY__c, LCY_YTD_Sales__c, LCY_YTD_Sales_QTY__c, LFY_PTD_Sales__c, LFY_PTD_Sales_QTY__c, LFY_YTD_Sales__c, LFY_YTD_Sales_QTY__c, Product_Family__c, Product_Group__c, Product_Line__c, Rolling_12_Months_Budget__c, Rolling_12_Months_LY_Budget__c, Rolling_12_Months_LY_Sales__c, Rolling_12_Months_LY_Sales_QTY__c, Rolling_12_Months_Sales__c, Rolling_12_Months_Sales_QTY__c, Rolling_52_Weeks_Budget__c, Rolling_52_Weeks_Sales__c, Rolling_52_Weeks_Sales_QTY__c, Sales_Rep_Name__c, Territory__c FROM Aggregated_Sales_Source__c';
		if(Test.isRunningTest()) {
			query += ' LIMIT 10';
		}
		
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Aggregated_Sales_Source__c> scope) {
		logs.add('Batch Class \'icBatchProcessAggregatedSalesSource\' executing batch ' + batchNumber + ' at : ' + Datetime.now());
		batchNumber++;		

		Set<String> setClientNumbers = new Set<String>();		

		for(Aggregated_Sales_Source__c source : scope) {
			setClientNumbers.add(source.Client_Number__c);
		}

		System.debug('Identified external Id (' + setClientNumbers + ')');

		List<Account> listAccounts = blAccounts.getListAccountByExternalId(setClientNumbers);
		Map<String, Account> mapAccountByExternalId = blAccounts.getMapAccountsByExternalId(listAccounts);

		List<Contact> listNovatechContacts = blContacts.getListContactForNovatech();

		List<Product2> listAllProducts = blProducts.getListAllProducts();

		System.debug('Fetched Accounts (' + listAccounts.size() + ')');
		System.debug('Fetched Contacts (' + listNovatechContacts.size() + ')');
		System.debug('Fetched Products (' + listAllProducts.size() + ')');

		List<Aggregated_Sales__c> listNewAggregatedSales = new List<Aggregated_Sales__c>();
		for(Aggregated_Sales_Source__c source : scope) {
			System.debug('Handling source "' + source.Client_Name__c + '" (' + source.Client_Number__c + ') with rep : ' + source.Sales_Rep_Name__c);
			Account thisSourceAccount = mapAccountByExternalId.get(source.Client_Number__c);
			if(thisSourceAccount != null) {
				Contact thisSourceContact = blContacts.getContactByNameString(listNovatechContacts, source.Sales_Rep_Name__c);
				Product2 thisSourceProduct = blProducts.getProductByGroupFamilyLine(listAllProducts, source.Product_Group__c.toLowerCase(), source.Product_Family__c.toLowerCase(), source.Product_Line__c.toLowerCase());
				System.debug('Found Accounts "' + thisSourceAccount + '"');
				System.debug('Found Contacts "' + thisSourceContact + '"');
				System.debug('Found Products "' + thisSourceProduct + '"');

				listNewAggregatedSales.add(blAggregatedSales.generateNewAggregatedSaleFromSource(source, thisSourceAccount, thisSourceContact, thisSourceProduct));
			}
		}

		totalCreated += listNewAggregatedSales.size();
		totalDeleted += scope.size();
		
		blAggregatedSales.createNewAggregatesSales(listNewAggregatedSales);
		//blAggregatedSales.deleteAggregatesSalesSource(scope);
	}

	global void finish(Database.BatchableContext BC) {
		logs.add('Batch Class \'icBatchProcessAggregatedSalesSource\' completed at : ' + Datetime.now());
		logs.add('Total created ' + totalCreated + ' records');
		logs.add('Total deleted ' + totalDeleted + ' records');

		Messaging.SingleEmailMessage emailOut = new Messaging.SingleEmailMessage(); 

		String emailBodyHTML = '';
		for(String log : logs)
		{
			emailBodyHTML += log + '<br>';
		}

		emailOut.setSubject('Batch Class \'icBatchProcessAggregatedSalesSource\' Execution Complete');
		emailOut.setSenderDisplayName('Apex Batch');
		emailOut.setToAddresses(new String[] {'denis.roy@in-cloud.ca'});
		emailOut.setHTMLBody(emailBodyHTML);        

		Messaging.sendEmail(new List<Messaging.Email>{emailOut});
	}
}