public class icBusinessLogicAccount implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Account> getListAccountByExternalId(Set<String> setExternalIds);
		Map<String, Account> getMapAccountsByExternalId(List<Account> listAccounts);
	}

	public class Impl implements IClass {
		icRepositoryAccount.IClass repoAccounts = (icRepositoryAccount.IClass) icObjectFactory.GetSingletonInstance('icRepositoryAccount');

		public List<Account> getListAccountByExternalId(Set<String> setExternalIds) {
			return repoAccounts.getListAccountByExternalId(setExternalIds);
		}

		public Map<String, Account> getMapAccountsByExternalId(List<Account> listAccounts) {
			Map<String, Account> returnMapAccounts = new Map<String, Account>();

			for(Account thisAccount : listAccounts) {
				returnMapAccounts.put(thisAccount.Oracle_Site_ID__c, thisAccount);
			}

			return returnMapAccounts;
		}
	}
}