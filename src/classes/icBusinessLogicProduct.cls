public class icBusinessLogicProduct implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Product2> getListAllProducts();
		Product2 getProductByGroupFamilyLine(List<Product2> listProduct, String productGroup, String productFamily, String productLine);
	}

	public class Impl implements IClass {
		icRepositoryProduct.IClass repoProducts = (icRepositoryProduct.IClass) icObjectFactory.GetSingletonInstance('icRepositoryProduct');
		
		public List<Product2> getListAllProducts() {
			return repoProducts.getListAllProducts();
		}

		public Product2 getProductByGroupFamilyLine(List<Product2> listProduct, String productGroup, String productFamily, String productLine) {
			Product2 returnProduct = null;

			//System.debug('Product : ' + productGroup + ' : ' + productFamily + ' : ' + productLine);

			for(Product2 thisProduct : listProduct) {
				//System.debug('Comparing : ' + thisProduct.Family + ' : ' + thisProduct.Name + ' : ' + thisProduct.Product_Line__c);
				if(thisProduct.Name.toLowerCase() == productFamily 
					&& thisProduct.Family.toLowerCase() == productGroup 
					&& thisProduct.Product_Line__c.toLowerCase() == productLine) 
				{
					returnProduct = thisProduct;
					break;
				}
			}

			return returnProduct;
		}
	}
}

