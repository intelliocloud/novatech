public class icRepositoryAggregatedSales implements icIClass {
	
	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		void createNewAggregatesSales(List<Aggregated_Sales__c> listNewAggregatedSales);
		void deleteAggregatesSales(List<Aggregated_Sales__c> listAggregatedSales);
		void deleteAggregatesSalesSource(List<Aggregated_Sales_Source__c> listAggregatedSalesSource);
	}

	public class Impl implements IClass {
		public void createNewAggregatesSales(List<Aggregated_Sales__c> listNewAggregatedSales) {
			insert listNewAggregatedSales;
		}

		public void deleteAggregatesSales(List<Aggregated_Sales__c> listAggregatedSales) {
			delete listAggregatedSales;
		}

		public void deleteAggregatesSalesSource(List<Aggregated_Sales_Source__c> listAggregatedSalesSource) {
			delete listAggregatedSalesSource;	
		}
	}
}